/*jshint esversion: 6 */ 
const moviesMap = new Map();

const getUrl = (url) =>{
    return moviesMap.get(url);
};

const setUrl = (url, json) =>{
    moviesMap.set(url, json);
    return moviesMap.get(url);
};

module.exports = {
    getUrl: getUrl,
    setUrl: setUrl
};