/*jshint esversion: 6 */ 
const http = require("http");
const axios = require("axios");
const cache = require("./cache");
const pjson = require("./package.json");
const port = 3003;
const url = "http://www.omdbapi.com/?apikey=";
const apiKey = "4fd63119";
let cacheCount = 0;
let pullCount = 0;

const requestHandler = (request, response) => {
  if(request.url == "/-/info"){
    const answer = {
                    name: pjson.name,
                    version: pjson.version,
                    requestCount: cacheCount + pullCount,
                    cacheHitRatio: cacheCount / (cacheCount + pullCount)
                  };
    response.end(JSON.stringify(answer));
  }

  else{
    const movieData = cache.getUrl(request.url);
    
    if (movieData == undefined){
      pullCount++;
      axios.get(`${url}${apiKey}&${request.url.substring(2)}`)
      .then(function (res) {
        const jsonData = JSON.stringify(res.data);

        cache.setUrl(request.url, jsonData);
        response.end(jsonData);
      })
      .catch((err) => {
        console.error(err);
        response.statusCode = err.response.status;
        response.end(JSON.stringify(err.response.data));  
      });
    }

    else {
      cacheCount++;
      response.end(movieData);
    }
  }
};

const server = http.createServer(requestHandler);

server.listen(port, (err) => {
  if (err) {
    return console.error('something bad happened', err);
  }

  console.log(`server is listening on ${port}`);
});


